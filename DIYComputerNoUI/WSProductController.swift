//
//  WSProductController.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/19.
//  Copyright © 2016年 forever. All rights reserved.
//

import Foundation
import Alamofire

typealias Block = (String, WSProductItem) -> ()

class WSProductController: WSController {
	var title: String!
	var url: String!
	
	// 产品列表
	private var dataArray = Array<WSProductItem>()
	private var request: NSURLRequest?
	// 标记是否退出
	private var isGoBack = false
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// 实例化数据原数组
		self.createDataArray()
		// 下载，解析，存储
		self.requestData()
		// 搭建UI，显示数据
		self.refresh()
		
		// 如果没有退出，循环继续
		while (isGoBack == false) {
			self.scanData()
		}
	}
	
	func createDataArray() {
		if dataArray.count > 0 {
			dataArray.removeAll()
		}
	}
	
	func requestData() {
//		Alamofire.request(.GET, "https://httpbin.org/get")
//		.response(completionHandler: { (request, response, data, error) in
//			print(request, response, data, error)
//			
//		})
//		Alamofire.request("https://httpbin.org/get").responseJSON { response in
//			if let JSON = response.result.value as? [String: AnyObject] {
//				print(JSON)
//			}
//		}
		let request = Alamofire.request(.GET, "https://httpbin.org/get")
		print(request)
//		request.responseString { response in
//			
//			if let response = response.response {
//				print(response)
//			}
//			
//		}
//		
//		Alamofire.request(.GET, "https://httpbin.org/get", parameters: ["foo": "bar"])
//			.responseJSON { response in
//				debugPrint(response)     // prints detailed description of all response properties
//				
//				print(response.request)  // original URL request
//				print(response.response) // URL response
//				print(response.data)     // server data
//				print(response.result)   // result of response serialization
//				
//				if let JSON = response.result.value {
//					print("JSON: \(JSON)")
//				}
//		}
		let webUrl = NSURL.init(string: url!)
		let urlSession = NSURLSession.sharedSession().dataTaskWithURL(webUrl!) { (responseData, response, error) in
			if error == nil {
				
				print(responseData!)
//				print(NSJSONSerialization.JSONObjectWithData(responseData!, options: .AllowFragments))
				
				print(response!)
			}else {
				
			}
		}
		
		urlSession.resume()
		
	}
	
	// 刷新UI
	func refresh() {
		var str = String.init(format: "\n选择%@产品\n", self.title)
		var i = 1
		for item in dataArray {
			str = str.stringByAppendingFormat("[%ld]%@\n", i, item.description)
			i += 1
		}
		print(str)
	}
	
	// 读取输入
	func scanData() {
		
		print("请输入产品序号，选择产品。输入0进行刷新，输入-1，返回上一页面");
		let ctrl = Int.init(readLine()!)
		if (ctrl <= dataArray.count && ctrl >= 1) {
			// 选中了某种商品
			self.choseProduct(ByNumber: ctrl!)
		} else if (ctrl == -1) {
			// 返回上一页面
			isGoBack = true;
		} else if (ctrl == 0) {
			self.createDataArray()
			self.requestData()
		} else {
			print("输入错误，请重试!");
		}
	}
	// 选择某种产品
	func choseProduct(ByNumber number: NSInteger) {
		
		// 找到选中的产品
		let item = dataArray[number - 1]
		// 将这个数据发送到前一页
		block!(title, item)
		// 调用block就是调用DeviceViewController的代码，参数也就传到了DeviceViewController的代码里。
		
		// 返回上一页
		isGoBack = true
	}

	//数据发送方，声明block引用
	var block: Block?
	
}
