//
//  WSDeviceController.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/19.
//  Copyright © 2016年 forever. All rights reserved.
//

import Foundation

class WSDeviceController: WSController {

	lazy var dataDict : Dictionary<String, WSProductItem?> = {
		var itemDict = [String: WSProductItem?]()
		for itemType in itemTypes {
			itemDict.updateValue(nil, forKey: itemType)
		}
		
		return itemDict
	}()
	
	var totlePrice: Int {
		get {
			var price = 0
			for (_, itemProduct) in dataDict {
				if itemProduct != nil {
					price += Int.init((itemProduct?.price)!)!
				}
			}
			
			return price
		}
	}
	
	override func viewDidLoad() {
		
		self.refresh()
		
		while (true) {
			self.scanData();
		}
	}
	
	func refresh() {
		
		var str = "\n"
		var i = 1
		var productInfo = ""
		
		for itemType in itemTypes {
			let productItem = dataDict[itemType]!
			
			if productItem == nil {
				productInfo = "(暂未选择设备)"
			}else {
				productInfo = productItem!.description
			}
			str = str.stringByAppendingFormat("[%ld][%@]%@\n", i, itemType, productInfo)
			i += 1
		}
		// 添加合计金额
		str = str.stringByAppendingFormat("合计金额:￥%lu\n", totlePrice)
		
		print(str)
	}
	
	func scanData() {
		print("\n请输入操作，输入设备序号，挑选设备。输入0，刷新列表。")
		let ctrl = Int.init(readLine()!)
		
		if (ctrl <= itemTypes.count && ctrl >= 1) {
			// 推出第二页
			self.pushProductViewController(ByNumber:ctrl!)
		} else if (ctrl == 0) {
			self.refresh()
		} else {
			print("\n输入错误，请重试!")
		}
	}
	
	func pushProductViewController(ByNumber number: Int) {
		let pvc = WSProductController.init()
		// 给第二页赋值
		pvc.title = itemTypes[number - 1]
		pvc.url = WSTools.urlFrom(deviceName: pvc.title)
		// 如果网址不存在，比如title是@“显卡”
		if(pvc.url == nil) {
			print("该设备暂不提供选择!")
			return
		}
		
		pvc.block = {
			(title: String, product: WSProductItem) -> () in
			//CPU  PSBCPUItem
			//将返回来的数据存入字典
			self.dataDict.removeValueForKey(title)
			self.dataDict.updateValue(product, forKey: title)
		}
		
		/*
		*	在DVC中向PVC传值了，如title，url，block。上一栈的页面向后一栈传值，直接用属性赋值就可以了。
		*	后一栈向前一栈传值，需要使用回调。因此称作反向传值。
		*/
		pvc.viewDidLoad()
		
		//从第二页回来
		self.refresh()
	}
}
