//
//  Tools.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/19.
//  Copyright © 2016年 forever. All rights reserved.
//

import Foundation

let itemTypes = ["CPU", "主板", "内存", "硬盘", "显卡", "机箱"]
let itemClass = ["CPU": "PSBCPUItem", "内存": "PSBMemeryItem", "主板": "PSBMainboardItem", "硬盘": "PSBDiskItem"];


let CPU_PRO_URL = "http://lib3.wap.zol.com.cn/index.php?c=IphoneDiy_ProList&priceId=noPrice&isiphone=1&retina=2&vs=V1.2&page=1&locationId=1&subcateId=28&queryType=&keyword=&manuId=&paramStr=&priceId=noPrice%20HTTP/1.1%20=%3E%20HTTP/1.1%20200%20OK%20[23.445%20s]"

let MEMERY_PRO_URL = "http://lib3.wap.zol.com.cn/index.php?c=IphoneDiy_ProList&priceId=noPrice&isiphone=1&retina=2&vs=V1.2&page=1&locationId=1&subcateId=3&queryType=&keyword=&manuId=¶mStr=&priceId=noPrice"

let MAINBOARD_PRO_URL = "http://lib3.wap.zol.com.cn/index.php?c=IphoneDiy_ProList&priceId=noPrice&isiphone=1&retina=2&vs=V1.2&page=1&locationId=1&subcateId=5&queryType=&keyword=&manuId=¶mStr=&priceId=noPrice"

let DISK_PRO_URL = "http://lib3.wap.zol.com.cn/index.php?c=IphoneDiy_ProList&priceId=noPrice&isiphone=1&retina=2&vs=V1.2&page=1&locationId=1&subcateId=2&queryType=&keyword=&manuId=¶mStr=&priceId=noPrice"
let itemUrl = ["CPU": CPU_PRO_URL, "内存": MEMERY_PRO_URL, "主板": MAINBOARD_PRO_URL, "硬盘": DISK_PRO_URL]



class WSTools: NSObject {
	
	
	/**根据设备名，返回产品的URL*/
	class func urlFrom(deviceName name: String) -> String? {
		let dict = itemUrl
		
		return dict[name]
	}
	
	/**根据设备名，返回产品的类*/
	class func modelClassFrom(deviceName name:String) -> AnyClass? {
		let dict = itemClass
		
		return NSClassFromString(dict[name]!)
	}
	
}