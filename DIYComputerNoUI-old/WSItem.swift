//
//  WSItem.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/19.
//  Copyright © 2016年 forever. All rights reserved.
//

import Foundation

class WSItem: NSObject {
	
}


class WSProductItem: WSItem {
	var proId = ""
	var name = ""
	var price = ""
	
	override var description: String {
		return name
	}
	
}


class WSCPUItem: WSProductItem {
	var mainFreq = ""
	
	override var description: String {
		return mainFreq
	}
}

class PSBMemeryItem: WSProductItem {
	var memerySize = ""
	
}

class PSBMainboardItem: WSProductItem {
	var chipset = ""
	
}

class PSBDiskItem: WSProductItem {
	var diskSize = ""
	
}

