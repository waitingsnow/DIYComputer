//
//  UIColor+FLExtensions.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/18.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

extension UIColor {
	class func randomColor() -> UIColor {
		let a = random(to: 128) / 255.0 + 0.5
		let b = random(to: 128) / 255.0 + 0.5
		let c = random(to: 128) / 255.0 + 0.5
		return UIColor.init(colorLiteralRed: a, green: b, blue: c, alpha: 1)
	}
	
	
	class func backgroundColor() -> UIColor {
		return UIColor.init(white: 0.88, alpha: 1)
	}
}

func random(from number1: UInt32, to number2: UInt32) -> Float {
	return Float(arc4random_uniform(number2) + number1)
}

func random(to number:UInt32) -> Float {
	return random(from: 0, to: number)
}

