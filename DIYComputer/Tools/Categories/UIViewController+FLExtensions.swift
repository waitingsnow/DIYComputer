//
//  UIViewController+FLExtensions.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/21.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

extension UIViewController {
	
	var statusBarHeight: CGFloat {
		
//		if let window = view.window {
			let statusBarFrame = UIApplication.sharedApplication().keyWindow!.convertRect(UIApplication.sharedApplication().statusBarFrame, toView: view)
			return statusBarFrame.height
			
//		} else {
//			return 0
//		}
	}
	
	var navigationBarHeight: CGFloat {
		
		if let navigationController = navigationController {
			return navigationController.navigationBar.frame.height
			
		} else {
			return 0
		}
	}
	
	var topBarsHeight: CGFloat {
		return statusBarHeight + navigationBarHeight
	}
	
	
	var tabBarHeight: CGFloat {
		if let tabBarController = tabBarController {
			return tabBarController.tabBar.frame.height
		}else {
			return 0
		}
	}
}