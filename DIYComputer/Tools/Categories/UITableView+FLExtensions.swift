//
//  UITableView+FLExtensions.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/21.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

protocol NibLoadable {
	
	static var ws_nibName: String { get }
}

extension UITableViewCell: NibLoadable {
	
	static var ws_nibName: String {
		return String(self)
	}
}

extension UICollectionReusableView: NibLoadable {
	
	static var ws_nibName: String {
		return String(self)
	}
}

extension UITableViewHeaderFooterView: NibLoadable {
	
	static var ws_nibName: String {
		return String(self)
	}
}

protocol Reusable: class {
	
	static var ws_reuseIdentifier: String { get }
}

extension UITableViewCell: Reusable {
	
	static var ws_reuseIdentifier: String {
		return String(self)
	}
}

extension UITableViewHeaderFooterView: Reusable {
	
	static var ws_reuseIdentifier: String {
		return String(self)
	}
}

extension UICollectionReusableView: Reusable {
	
	static var ws_reuseIdentifier: String {
		return String(self)
	}
}


extension UITableView {
	
	func registerClassOf<T: UITableViewCell where T: Reusable>(_: T.Type) {
		
		registerClass(T.self, forCellReuseIdentifier: T.ws_reuseIdentifier)
	}
	
	func registerNibOf<T: UITableViewCell where T: Reusable, T: NibLoadable>(_: T.Type) {
		
		let nib = UINib(nibName: T.ws_nibName, bundle: nil)
		registerNib(nib, forCellReuseIdentifier: T.ws_reuseIdentifier)
	}
	
	func registerHeaderFooterClassOf<T: UITableViewHeaderFooterView where T: Reusable>(_: T.Type) {
		
		registerClass(T.self, forHeaderFooterViewReuseIdentifier: T.ws_reuseIdentifier)
	}
	
	func registerHeaderFooterNibOf<T: UITableViewHeaderFooterView where T: Reusable, T: NibLoadable>(_: T.Type) {
		
		let nib = UINib(nibName: T.ws_nibName, bundle: nil)
		registerNib(nib, forHeaderFooterViewReuseIdentifier: T.ws_reuseIdentifier)
	}
	
	func dequeueReusableCell<T: UITableViewCell where T: Reusable>() -> T {
		
		guard let cell = dequeueReusableCellWithIdentifier(T.ws_reuseIdentifier) as? T else {
//			fatalError("Could not dequeue cell with identifier: \(T.ws_reuseIdentifier)")
			return T.init(style: UITableViewCellStyle.Value1, reuseIdentifier: T.ws_reuseIdentifier)
		}
		
		return cell
	}
	
	func dequeueReusableHeaderFooter<T: UITableViewHeaderFooterView where T: Reusable>() -> T {
		
		guard let view = dequeueReusableHeaderFooterViewWithIdentifier(T.ws_reuseIdentifier) as? T else {
//			fatalError("Could not dequeue HeaderFooter with identifier: \(T.ws_reuseIdentifier)")
			return T.init(reuseIdentifier: T.ws_reuseIdentifier)
		}
		
		return view
	}
}
