//
//  WSScrollViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/13.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire

public class WSScrollViewController: WSViewController {
	// 懒加载设置滚动视图的内容视图
	private(set) lazy var contentView : UIView! = {
		let contentView = UIView.init(frame: CGRectZero)
		contentView.backgroundColor = UIColor.randomColor()
		return contentView
	}()
	// 懒加载滚动视图，并设置相关属性
	private lazy var scrollView : UIScrollView! = {
		//....
		let scrollView = UIScrollView.init(frame: CGRectZero)
		scrollView.showsHorizontalScrollIndicator = false
		scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.OnDrag
		return UIScrollView.init(frame: CGRectZero)
	}()
	// 可以在外部设置滚动视图的 inset
	var contentInset : UIEdgeInsets! = UIEdgeInsetsMake(64, 0, 0, 0) {
		didSet {
			scrollView.contentInset = contentInset
			scrollView.scrollIndicatorInsets = contentInset
			scrollView.contentOffset = CGPointMake(0, -contentInset.top)
			scrollView.backgroundColor = UIColor.init(white: 0.88, alpha: 1.0)
			contentView.snp_updateConstraints { (make) in
				make.height.greaterThanOrEqualTo(view).offset(1 - contentInset.top - contentInset.bottom)
			}
		}
	}
	
    override public func viewDidLoad() {
        super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		loadContentView()
		contentInset = UIEdgeInsetsMake(self.topBarsHeight, 0, self.tabBarController?.tabBar.hidden == true || self.hidesBottomBarWhenPushed == true ? 0 : self.tabBarHeight, 0)
    }
	// 设置滚动视图和内容视图的约束
	private func loadContentView() {
		view.addSubview(scrollView)
		scrollView.addSubview(contentView)
		contentView.snp_makeConstraints { (make) in
			make.edges.equalTo(scrollView)
			make.width.equalTo(scrollView)
			make.height.greaterThanOrEqualTo(view).offset(1 - contentInset.top - contentInset.bottom)
		}
		
		scrollView.snp_makeConstraints { (make) in
			make.edges.equalTo(view)
			make.bottom.equalTo(contentView)
		}
	}
	
	override public func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
		var contentInset = self.contentInset
		
		if size.width > size.height {
			contentInset.top = self.navigationBarHeight
		}else {
			contentInset.top = self.navigationBarHeight + self.statusBarHeight
		}
		
		self.contentInset = contentInset
	}

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
