//
//  WSViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/13.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit
import VBFPopFlatButton

public class WSViewController: UIViewController {
	
	deinit {
		print("\(self) deinit")
	}
	
	override public func viewDidLoad() {
		super.viewDidLoad()
		// 设置控制器中的滚动视图不主动适配导航条和 tabBar
		self.automaticallyAdjustsScrollViewInsets = false
		self.view.backgroundColor = UIColor.randomColor()
		
		if self.navigationController?.viewControllers.count == 1 && self.navigationController?.presentingViewController != nil {
			self.createCustomBackItem()
		}else {
			
		}
		
	}
	
	func createCustomBackItem() {
		/**
		*  自定义返回按钮
		*/
		let backBtn = VBFPopFlatButton.init(frame: CGRectMake(0, 0, 22, 22), buttonType: FlatButtonType.buttonBackType, buttonStyle: FlatButtonStyle.buttonPlainStyle, animateToInitialState: true)
		backBtn.addTarget(self, action: #selector(WSViewController.viewDidLoad), forControlEvents: UIControlEvents.TouchUpInside)
		let backItem = UIBarButtonItem.init(customView: backBtn)
		
		let naviSpacer = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
		naviSpacer.width = -16;
		
		self.navigationItem.leftBarButtonItems = [naviSpacer, backItem];
	}
	
	// 返回上一个控制器
	func returnLastViewController() {
		if self.navigationController?.popViewControllerAnimated(true) == nil {
			self.navigationController?.dismissViewControllerAnimated(true, completion: {
				
			})
		}
	}
	
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override public func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
	*/

}
