//
//  WSTableViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/13.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit
import SnapKit

public class WSTableViewController: WSViewController {
//	public override var searchDisplayController: UISearchDisplayController?
	internal var tableViewStyle = UITableViewStyle.Plain
	
	internal lazy var requestParameter : [String: String] = {
		return [
			"c": "IphoneDiy_CateList",
			"noParam": "1",
			"isiphone": "1",
			"retina": "2",
			"vs": "V1.2",
		]
	}()
	
	public var contentInset : UIEdgeInsets = UIEdgeInsetsMake(64, 0, 0, 0) {
		didSet {
			tableView.contentInset = contentInset
			tableView.scrollIndicatorInsets = contentInset
		}
	}
	
	public lazy var cellModels : [AnyObject] = {
		return []
	}()
	
	public init(style: UITableViewStyle) {
		super.init(nibName: nil, bundle: nil)
		tableViewStyle = style
	}
	
	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
	}
	
	private(set) lazy var tableView : UITableView! = {
		let style = self.tableViewStyle
		let tableView = UITableView.init(frame: CGRectZero, style: style)
		tableView.showsHorizontalScrollIndicator = false
		tableView.backgroundColor = UIColor.backgroundColor()
		tableView.rowHeight = 64
		tableView.frame = self.view.frame
		tableView.tableFooterView = UIView.init()
		tableView.delegate = self
		tableView.dataSource = self
		
		return tableView
	}()
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		view.addSubview(tableView)
		tableView.snp_makeConstraints { (make) in
			make.edges.equalTo(view)
		}
		contentInset = UIEdgeInsetsMake(self.topBarsHeight, 0, self.tabBarController?.tabBar.hidden == true || self.hidesBottomBarWhenPushed == true ? 0 : self.tabBarHeight, 0)
	}
	
	override public func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
		super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
		var contentInset = self.contentInset
		
		if size.width > size.height {
			contentInset.top = self.navigationBarHeight
		}else {
			contentInset.top = self.navigationBarHeight + self.statusBarHeight
		}
		
		self.contentInset = contentInset
	}
}


// MARK: - UITableViewDataSource, UITableViewDelegat

extension WSTableViewController: UITableViewDataSource, UITableViewDelegate {
	
	public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 0
	}
	
	public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		return UITableViewCell()
	}
}


