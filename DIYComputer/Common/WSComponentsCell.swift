//
//  WSComponetsCell.swift
//  DIYComputer
//
//  Created by 王广威 on 2016/9/22.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

class WSComponentsCell: UITableViewCell {

	@IBOutlet weak var componentTitle: UILabel!
	
	@IBOutlet weak var componentDetail: UILabel!
	
	@IBOutlet weak var componentNumber: UILabel!
	
	@IBOutlet weak var componentPrice: UILabel!
	
	
	func configureWithComponentDetail(componentDetailModle: WSComponentDetailModel) {
		componentTitle.text = componentDetailModle.subcateName
		componentNumber.text = "x\(componentDetailModle.diyNum)"
		componentDetail.text = componentDetailModle.simpleProName
		componentPrice.text = componentDetailModle.diyPrice
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
