//
//  WSTotalFooterView.swift
//  DIYComputer
//
//  Created by 王广威 on 2016/9/23.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

class WSTotalFooterView: UITableViewHeaderFooterView {
	
	@IBOutlet weak var totalPrice: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.contentView.backgroundColor = UIColor.whiteColor()
		
	}
	
	func configureWithComponents(model: WSDIYComponentsModel) {
		
		totalPrice.text = "￥\(model.price)"
	}
	
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
