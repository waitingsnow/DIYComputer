//
//  SettingViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/21.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

class SettingViewController: WSTableViewController {
	
	override init(style: UITableViewStyle) {
		super.init(style: UITableViewStyle.Grouped)
	}
	
	required internal init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		tableViewStyle = UITableViewStyle.Grouped
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
		self.cellModels = [
			[
				[
					"title": "ZOL账号设置",
					"detail": "轻触此处登录",
				],
				[
					"title": "微博账号设置",
					"detail": "轻触此处登录",
				],
			],
			[
				[
					"title": "建议反馈",
				],
				[
					"title": "关于我们",
				],
			],
		]
		
		self.tableView.rowHeight = 56
		self.tableView.sectionHeaderHeight = 28
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}

extension SettingViewController {
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return self.cellModels.count
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let cellArr = self.cellModels[section]
		
		return cellArr.count
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell : UITableViewCell = tableView.dequeueReusableCell()
		
		let cellArr = self.cellModels[indexPath.section] as! [[String: String]]
		let cellInfo = cellArr[indexPath.row]
		cell.textLabel?.textColor = UIColor.darkTextColor()
		cell.textLabel?.font = UIFont.systemFontOfSize(15)
		cell.detailTextLabel?.font = UIFont.systemFontOfSize(14)
		cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
		
		cell.textLabel?.text = cellInfo["title"]
		cell.detailTextLabel?.text = cellInfo["detail"]
		
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		defer {
			tableView.deselectRowAtIndexPath(indexPath, animated: true)
		}
		
	}
	
}
