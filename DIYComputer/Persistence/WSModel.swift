//
//  WSModel.swift
//  DIYComputer
//
//  Created by 王广威 on 2016/9/22.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit
import YYKit

class WSItem: NSObject {
	
}

class WSProductItem: WSItem {
	var proId = ""
	var name = ""
	var price = ""
	
	override var description: String {
		return name
	}
	
}

class WSCPUItem: WSProductItem {
	var mainFreq = ""
	
	override var description: String {
		return mainFreq
	}
}

class PSBMemeryItem: WSProductItem {
	var memerySize = ""
	
}

class PSBMainboardItem: WSProductItem {
	var chipset = ""
	
}

class PSBDiskItem: WSProductItem {
	var diskSize = ""
	
}

class WSRankModel: NSObject {
	var id = ""
	var title = ""
	var price = ""
	var time = ""
	var isShop = ""
	var merchantId = ""
	var userId = ""
	var url = ""
	
}

class WSDIYComponentsModel: NSObject {
	var userId = ""
	var cateId = ""
	var cateName = ""
	var title = ""
	var price = ""
	var time = ""
	var nickName = ""
	var score = ""
	var diyNum = ""
	
	static func modelCustomPropertyMapper() -> [String : AnyObject]? {
		return [
			"nickName": "uesrInfo.nickName",
			"score": "uesrInfo.score",
		]
	}
	
}

class WSComponentDetailModel: NSObject {
	var subcateName = ""
	var simpleProName = ""
	var diyPrice = ""
	var diyNum = ""
	
	
}


class WSProductPriceShow: NSObject {
	var price = 0
	var mark = "￥"
	var style = ""
	var style2 = "price-normal"
	var hidden = ""
	var hiddens = ""
	var showDate = 0
	var noTime =  0
	var noSale = 0
}

class WSProductModel: WSItem {
	var proId = ""
	var name = ""
	var price = ""
	var highPrice = ""
	var lowPrice = ""
	var level = ""
	var manuId = ""
	var picSrc = ""
	var proUrl = ""
	var shorName = ""
	var priceShow : WSProductPriceShow!
	
	
	//	baseParam: {
	//	适用网络类型: {
	//	1: "万兆以太网",
	//	3: 595,
	//	5: 1,
	//	6: 125,
	//	7: 1
	//	},
	//	传输速率: {
	//	1: "10000Mbps",
	//	2: 2769,
	//	3: 592,
	//	5: 1,
	//	6: 110,
	//	7: 1
	//	},
	//	总线类型: {
	//	1: "PCI-E",
	//	2: 2764,
	//	3: 590,
	//	5: 1,
	//	6: 120,
	//	7: 1
	//	},
	//	网线接口类型: {
	//	1: "RJ-45",
	//	2: 2766,
	//	3: 589,
	//	5: 1,
	//	6: 115,
	//	7: 1
	//	}
	//	},
}



