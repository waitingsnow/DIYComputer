//
//  WSRankTableViewCell.swift
//  DIYComputer
//
//  Created by 王广威 on 2016/9/22.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

class WSRankTableViewCell: UITableViewCell {

	@IBOutlet weak var rankNumber: UIButton!
	
	@IBOutlet weak var rankTitle: UILabel!
	
	@IBOutlet weak var rankDetail: UILabel!
	
	@IBOutlet weak var rankPrice: UILabel!
	
	
	func configureWithRankModel(rankModel: WSRankModel, andIndex index:Int) {
		rankTitle.text = rankModel.title
		rankDetail.text = rankModel.time
		rankPrice.text = "￥\(rankModel.price)"
		let rank = index >= 3 ? 5 : index + 1
		
		rankNumber.setBackgroundImage(UIImage.init(imageLiteral: "bg_no_\(rank)"), forState: UIControlState.Normal)
		rankNumber.setTitle("\(index + 1)", forState: UIControlState.Normal)
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//		self.separatorInset = UIEdgeInsetsMake(0, 48, 0, 0)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
