//
//  WSRankViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 2016/9/22.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WSRankViewController: WSTableViewController {
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		self.getRankData()
		
		self.tableView.rowHeight = 72
		self.tableView.registerNibOf(WSRankTableViewCell)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func getRankData() {
		
		Alamofire.request(.GET, "http://lib3.wap.zol.com.cn/index.php", parameters: requestParameter, encoding: ParameterEncoding.URL, headers: nil).responseJSON { (response) in
			switch response.result
			{
			case .Success:
				if let models = NSArray.modelArrayWithClass(WSRankModel.self, json: response.result.value!) {
					self.cellModels.appendContentsOf(models)
					self.tableView.reloadData()
				}
				break
			case .Failure:
				
				break
			}
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WSRankViewController {
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return cellModels.count
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell : WSRankTableViewCell = tableView.dequeueReusableCell()
		
		let cellModel = self.cellModels[indexPath.row] as! WSRankModel
		cell.configureWithRankModel(cellModel, andIndex: indexPath.row)
		
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		defer {
			tableView.deselectRowAtIndexPath(indexPath, animated: true)
		}
		
		let componetsVC = WSComponentsViewController.init(style: UITableViewStyle.Plain)
		let cellModel = self.cellModels[indexPath.row] as! WSRankModel
		componetsVC.requestParameter.updateValue(cellModel.id, forKey: "mainId")
		componetsVC.title = cellModel.title
		
		self.navigationController?.pushViewController(componetsVC, animated: true)
	}
	
}
