//
//  WSHotViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/21.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

class WSHotViewController: WSTableViewController {
	
	override init(style: UITableViewStyle) {
		super.init(style: UITableViewStyle.Grouped)
	}
	
	required internal init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		tableViewStyle = UITableViewStyle.Grouped
	}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		self.cellModels = [
			[
				"sectionTitle": "电脑配件方案排行",
				"cellInfo": [
					"Intel CPU方案",
					"AMD CPU方案",
					"NVIDIA 显卡方案",
					"AMD 显卡方案",
					"集成显卡方案",
				],
			],
			[
				"sectionTitle": "价格方案排行",
				"cellInfo": [
					"3000元以下",
					"3000-4000元",
					"4000-5000元",
					"5000-6000元",
					"6000-7000元",
					"7000-8000元",
					"8000元以上",
				],
			],
			[
				"sectionTitle": "配置类型排行",
				"cellInfo": [
					"经济实惠型",
					"家用学习型",
					"豪华发烧型",
					"疯狂游戏型",
					"商务办公型",
					"图形音像型",
					"网吧游戏型",
				],
			],
			[
				"sectionTitle": "热门配置排行",
				"cellInfo": [
					"最受关注电脑配置排行",
					"投票最多电脑排行",
					"差评最多电脑排行",
				],
			],
		]
		
		self.tableView.rowHeight = 44
		self.tableView.sectionFooterHeight = CGFloat.min
		self.tableView.registerClassOf(UITableViewCell)
		
		self.setUpNaviItem()
    }
	
	func setUpNaviItem() {
		
		let rightItem = UIBarButtonItem.init(title: "攒机", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.switchToDIY))
		self.navigationItem.rightBarButtonItem = rightItem
	}
	
	func switchToDIY() {
		self.tabBarController?.selectedIndex = 1
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WSHotViewController {
	
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return self.cellModels.count
	}
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let cellArray = self.cellModels[section]["cellInfo"] as! [String]
		
		return cellArray.count
	}
	
	func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		let sectionTitle = self.cellModels[section]["sectionTitle"] as! String
		return sectionTitle
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell()
		
		let cellArray = self.cellModels[indexPath.section]["cellInfo"] as! [String]
		
		cell.textLabel?.textColor = UIColor.darkTextColor()
		cell.textLabel?.font = UIFont.systemFontOfSize(16)
		cell.textLabel?.text = cellArray[indexPath.row]
		
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		defer {
			tableView.deselectRowAtIndexPath(indexPath, animated: true)
		}
		let rankParas = ["planId", "priceId", "cateId", "hotType"]
		
		
		let rankVC = WSRankViewController.init(style: UITableViewStyle.Plain)
		rankVC.requestParameter.updateValue(String(indexPath.row + 1), forKey: rankParas[indexPath.section])
		rankVC.hidesBottomBarWhenPushed = true
		
		let cellArray = self.cellModels[indexPath.section]["cellInfo"] as! [String]
		rankVC.title = cellArray[indexPath.row]
		
		self.navigationController?.pushViewController(rankVC, animated: true)
	}
	
}
