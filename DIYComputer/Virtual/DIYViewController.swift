//
//  DIYViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/21.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

class DIYViewController: WSTableViewController {
	
	override func viewDidLoad() {
		super.viewDidLoad()

		// Do any additional setup after loading the view.
		self.cellModels = [
			[
				"subcateTitle": "CPU",
				"subcateId": "28",
				"isNecessary": "",
			],
			[
				"subcateTitle": "主板",
				"subcateId": "5",
				"isNecessary": "",
			],
			[
				"subcateTitle": "内存",
				"subcateId": "3",
				"isNecessary": "",
			],
			[
				"subcateTitle": "硬盘",
				"subcateId": "2",
				"isNecessary": "",
			],
			[
				"subcateTitle": "固态硬盘",
				"subcateId": "626",
				"isNecessary": "",
			],
			[
				"subcateTitle": "显卡",
				"subcateId": "6",
				"isNecessary": "",
			],
			[
				"subcateTitle": "机箱",
				"subcateId": "10",
				"isNecessary": "",
			],
			[
				"subcateTitle": "电源",
				"subcateId": "35",
				"isNecessary": "",
			],
			[
				"subcateTitle": "散热器",
				"subcateId": "67",
				"isNecessary": "",
			],
			[
				"subcateTitle": "显示器",
				"subcateId": "84",
				"isNecessary": "",
			],
			[
				"subcateTitle": "鼠标",
				"subcateId": "32",
				"isNecessary": "",
			],
			[
				"subcateTitle": "键盘",
				"subcateId": "33",
				"isNecessary": "",
			],
			[
				"subcateTitle": "键鼠套装",
				"subcateId": "100",
				"isNecessary": "",
			],
			[
				"subcateTitle": "音箱",
				"subcateId": "34",
				"isNecessary": "",
			],
			[
				"subcateTitle": "光驱",
				"subcateId": "96",
				"isNecessary": "",
			],
			[
				"subcateTitle": "声卡",
				"subcateId": "7",
				"isNecessary": "",
			],
			[
				"subcateTitle": "网卡",
				"subcateId": "42",
				"isNecessary": "",
			],
			[
				"subcateTitle": "操作系统",
				"subcateId": "121",
				"isNecessary": "",
			],
			[
				"subcateTitle": "办公软件",
				"subcateId": "172",
				"isNecessary": "",
			],
		]
		
		self.tableView.rowHeight = 56
		self.tableView.registerNibOf(DIYTableViewCell)
		
		self.setUpNaviItem()
	}
	
	func setUpNaviItem() {
		
		let rightItem = UIBarButtonItem.init(title: "发表", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.preparePost))
		self.navigationItem.rightBarButtonItem = rightItem
		
		let leftItem = UIBarButtonItem.init(title: "北京", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.chooseCity))
		self.navigationItem.leftBarButtonItem = leftItem
	}
	
	func preparePost() {
		let postConfVC = WSPostConfViewController.init()
		postConfVC.hidesBottomBarWhenPushed = true
		postConfVC.title = "提交装机配置单"
		self.navigationController?.pushViewController(postConfVC, animated: true)
	}
	func chooseCity() {
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}

extension DIYViewController {
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return cellModels.count
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell : DIYTableViewCell = tableView.dequeueReusableCell()
		
		let cellModel = self.cellModels[indexPath.row] as! [String: String]
		
		cell.configureWithProduct(cellModel["subcateTitle"]!)
		
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		defer {
			tableView.deselectRowAtIndexPath(indexPath, animated: true)
		}
		
		let productList = WSProductListViewController.init(style: UITableViewStyle.Plain)
		let cellModel = self.cellModels[indexPath.row] as! [String: String]
		productList.requestParameter.updateValue(cellModel["subcateId"]!, forKey: "subcateId")
		productList.requestParameter.updateValue("1", forKey: "page")
		productList.requestParameter.updateValue("IphoneDiy_ProList", forKey: "c")
		productList.requestParameter.updateValue("noPrice", forKey: "priceId")
		productList.requestParameter.updateValue("1", forKey: "locationId")
		
		productList.title = cellModel["subcateTitle"]!
		productList.hidesBottomBarWhenPushed = true
		
		self.navigationController?.pushViewController(productList, animated: true)
		
	}
	
}
