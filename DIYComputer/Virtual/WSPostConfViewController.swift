//
//  WSPostConfViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 2016/9/26.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit
//import SnapKit

class WSPostConfViewController: WSScrollViewController {
	
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillChangeFrameNotification, object: nil)
	}
	var confDescription : UIView!
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		self.setUpNaviItem()
		self.setUpSubViews()
	}
	
	func setUpNaviItem() {
		print(#function)
		let rightItem = UIBarButtonItem.init(title: "提交", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(self.postMyConf))
		self.navigationItem.rightBarButtonItem = rightItem
	}
	
	func postMyConf() {
		
	}
	
	func setUpSubViews() {
		let confType = UITextField.init()
		self.contentView.addSubview(confType)
		confType.snp_makeConstraints { (make) in
			make.left.equalTo(16)
			make.centerX.equalTo(0)
			make.top.equalTo(16)
			make.height.equalTo(48)
		}
		confType.placeholder = "选择配置类型"
		confType.font = UIFont.systemFontOfSize(15)
		confType.backgroundColor = UIColor.randomColor()
		
		let confName = UITextField.init()
		self.contentView.addSubview(confName)
		confName.snp_makeConstraints { (make) in
			make.left.equalTo(16)
			make.centerX.equalTo(0)
			make.top.equalTo(confType.snp_bottom).offset(16)
			make.height.equalTo(48)
		}
		confName.placeholder = "填写配置名称"
		confName.font = UIFont.systemFontOfSize(15)
		confName.backgroundColor = UIColor.randomColor()
		
		confDescription = UITextView.init()
		self.contentView.addSubview(confDescription)
		confDescription.snp_makeConstraints { (make) in
			make.left.equalTo(16)
			make.centerX.equalTo(0)
			make.top.equalTo(confName.snp_bottom).offset(16)
//			make.height.equalTo(48)
			make.bottom.equalTo(-48)
		}
//		confDescription. = "填写配置名称"
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.keyboardChange(WithNotify:)), name: UIKeyboardWillChangeFrameNotification, object: nil)
		
	}
	
	func keyboardChange(WithNotify noti: NSNotification) {
		print(noti)
		if let keyboardInfo = noti.userInfo {
			let keyboardFrame = keyboardInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue
			let animationDuration = keyboardInfo[UIKeyboardAnimationDurationUserInfoKey] as! Double
			confDescription.snp_updateConstraints(closure: { (make) in
				make.bottom.equalTo(-keyboardFrame.CGRectValue().size.height - 48)
			})
			
			UIView.animateWithDuration(animationDuration, animations: { 
				self.confDescription.layoutIfNeeded()
			})
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		super.touchesBegan(touches, withEvent: event)
		self.view.endEditing(true)
	}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
