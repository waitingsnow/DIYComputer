//
//  WSProductCell.swift
//  DIYComputer
//
//  Created by 王广威 on 2016/9/23.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit
import YYKit

class WSProductCell: UITableViewCell {

	@IBOutlet weak var headImage: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var price: UILabel!
	@IBAction func useProduct(sender: UIButton) {
		
	}
	func configureWith(Product model: WSProductModel) {
		self.nameLabel.text = model.name
		self.price.text = model.price
		self.headImage.setImageWithURL(NSURL.init(string: model.picSrc), placeholder: nil)
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
