//
//  DIYTableViewCell.swift
//  DIYComputer
//
//  Created by 王广威 on 16/9/21.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit

class DIYTableViewCell: UITableViewCell {

	@IBOutlet weak var product: UIButton!
	@IBOutlet weak var detail: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
		product.layer.cornerRadius = 4
		product.layer.borderColor = UIColor.lightGrayColor().CGColor
		product.layer.masksToBounds = true
		product.layer.borderWidth = 1
    }

//	override func prepareForReuse() {
//		super.prepareForReuse()
//		product.setTitle("", forState: UIControlState.Normal)
//		detail.text = ""
//	}
	
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func configureWithProduct(product: String) {
		self.product.setTitle(product, forState: UIControlState.Normal)
		self.detail.text = "轻触选择\(product)"
	}
    
}
