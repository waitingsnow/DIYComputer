//
//  WSProductListViewController.swift
//  DIYComputer
//
//  Created by 王广威 on 2016/9/23.
//  Copyright © 2016年 forever. All rights reserved.
//

import UIKit
import Alamofire

class WSProductListViewController: WSTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

		// Do any additional setup after loading the view.
		self.getListData()
		
		self.tableView.rowHeight = 72
		self.tableView.registerNibOf(WSProductCell)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	
	func getListData() {
		
		Alamofire.request(.GET, "http://lib3.wap.zol.com.cn/index.php", parameters: requestParameter, encoding: ParameterEncoding.URL, headers: nil).responseJSON { (response) in
			switch response.result
			{
			case .Success:
				if let models = NSArray.modelArrayWithClass(WSProductModel.self, json: response.result.value!) {
					self.cellModels.appendContentsOf(models)
					self.tableView.reloadData()
				}
				
				break
			case .Failure:
				
				break
			}
		} 
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension WSProductListViewController {
	
	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return cellModels.count
	}
	
	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell : WSProductCell = tableView.dequeueReusableCell()
		
		let cellModel = self.cellModels[indexPath.row] as! WSProductModel
		
		cell.configureWith(Product: cellModel)
		
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		defer {
			tableView.deselectRowAtIndexPath(indexPath, animated: true)
		}
		
		
		
	}
	
}
